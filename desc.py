from rdkit.Chem import Descriptors, MACCSkeys
from rdkit.Chem import AllChem as Chem


def make_descriptors_from_smiles(smiles):
    mol = Chem.MolFromSmiles(smiles)
    mol.Compute2DCoords()
    # MACCS -> Molecular ACCess System
    fps = MACCSkeys.GenMACCSKeys(mol)
    maccs = list(fps)
    desc = maccs
    desc.append(Descriptors.Chi0(mol))
    desc.append(Descriptors.Kappa1(mol))
    desc.append(Descriptors.MolLogP(mol))
    desc.append(Descriptors.PEOE_VSA1(mol))
    desc.append(Descriptors.PEOE_VSA2(mol))
    desc.append(Descriptors.PEOE_VSA3(mol))
    desc.append(Descriptors.PEOE_VSA4(mol))
    desc.append(Descriptors.PEOE_VSA5(mol))
    desc.append(Descriptors.PEOE_VSA6(mol))
    desc.append(Descriptors.PEOE_VSA7(mol))
    desc.append(Descriptors.PEOE_VSA8(mol))
    desc.append(Descriptors.PEOE_VSA9(mol))
    desc.append(Descriptors.PEOE_VSA10(mol))
    desc.append(Descriptors.PEOE_VSA11(mol))
    desc.append(Descriptors.PEOE_VSA12(mol))
    desc.append(Descriptors.PEOE_VSA13(mol))
    desc.append(Descriptors.PEOE_VSA14(mol))
    return desc
