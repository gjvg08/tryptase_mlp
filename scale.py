import numpy as np


def calc_means_sigma(x_train):
    # 184 features stored in the second dimension
    num_features = np.size(x_train, 1)
    means = []
    sigmas = []
    # calculate means and standard deviation for every feature
    for i in range(num_features):
        sigma = np.std(x_train[:, i])
        if sigma == 0:
            sigma = 1
        sigmas.append(sigma)
        means.append(np.mean(x_train[:, i]))
    return sigmas, means


def scaleData(data, sigmas, means):
    scaled_data = np.zeros(data.shape)
    for i in range(np.size(data, 1)):
        scaled_data[:, i] = [(x - means[i]) / sigmas[i] for x in data[:, i]]
    return np.array(scaled_data)


def scaleY(data, sigma, mean):
    scaled_data = [(x - mean) / sigma for x in data]
    return np.array(scaled_data)


def unScaleY(scaled_data, sigma, mean):
    unscaled_data = [x * sigma + mean for x in scaled_data]
    return np.array(unscaled_data)


def scale_all(x_train, x_valid, x_test, y_train, y_valid, y_test):
    sigmas, means = calc_means_sigma(x_train)

    X_train = scaleData(x_train, sigmas, means).astype('float32')
    X_valid = scaleData(x_valid, sigmas, means).astype('float32')
    X_test = scaleData(x_test, sigmas, means).astype('float32')

    y_sigma = np.std(y_train)
    y_mean = np.mean(y_train)

    Y_train = scaleY(y_train, y_sigma, y_mean).astype('float32')
    Y_valid = scaleY(y_valid, y_sigma, y_mean).astype('float32')
    Y_test = scaleY(y_test, y_sigma, y_mean).astype('float32')

    return X_train, X_valid, X_test, Y_train, Y_valid, Y_test, y_sigma, y_mean
