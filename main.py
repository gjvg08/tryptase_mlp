import tensorflow as tf
import numpy as np
from parser import load_data
from sklearn.model_selection import train_test_split
from scale import scale_all, unScaleY
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
import keras.backend as K
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import seaborn as sb


def meanError(y_true, y_pred):
    return K.mean(K.abs(y_true - y_pred))


if __name__ == '__main__':
    print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
    data_dir = "data"
    test_ratio = 0.2
    x_train, y_train, x_valid, y_valid = load_data(data_dir)
    x_valid, x_test, y_valid, y_test = train_test_split(x_valid, y_valid, test_size=test_ratio, random_state=42)

    X_train, X_valid, X_test, Y_train, Y_valid, Y_test, y_sigma, y_mean = scale_all(x_train, x_valid, x_test, y_train,
                                                                                    y_valid, y_test)

    network = Sequential()
    activation = 'relu'
    network.add(Dense(20, activation=activation, input_shape=(184,)))
    print("network layer: ", network.output_shape)

    network.add(Dense(8, activation=activation))
    print("network layer: ", network.output_shape)

    # output layer:
    network.add(Dense(1, activation='linear'))

    network.summary()

    network.compile(loss="mean_squared_error", optimizer="adam", metrics=[meanError])

    network.fit(X_train, Y_train, batch_size=64, epochs=85, verbose=0)

    trainScore = network.evaluate(X_train, Y_train, verbose=0)
    validScore = network.evaluate(X_valid, Y_valid, verbose=0)
    testScore = network.evaluate(X_test, Y_test, verbose=0)

    P_train = network.predict(X_train, batch_size=None)
    P_valid = network.predict(X_valid, batch_size=None)
    P_test = network.predict(X_test, batch_size=None)

    print("\n\n")
    print("Training Loss: ", trainScore[0])
    print("Training Mean Error:  ", trainScore[1])
    print("\n")
    print("Validation Loss: ", validScore[0])
    print("Validation Mean Error:  ", validScore[1])
    print("\n")
    print("Test Loss: ", testScore[0])
    print("Test Mean Error:  ", testScore[1])

    y_train = y_train.flatten()
    y_valid = y_valid.flatten()
    y_test = y_test.flatten()
    p_train = unScaleY(P_train.flatten(), y_sigma, y_mean)
    p_valid = unScaleY(P_valid.flatten(), y_sigma, y_mean)
    p_test = unScaleY(P_test.flatten(), y_sigma, y_mean)

    print("Pearson Korrelation, Training Data: ", pearsonr(y_train, p_train))
    print("Pearson Korrelation, Validation Data: ", pearsonr(y_valid, p_valid))
    print("Pearson Korrelation, Validation Data: ", pearsonr(y_test, p_test))

    fig = plt.figure()
    ax = plt.axes()
    plt.xlabel('true')
    plt.ylabel('predicted')
    ax.plot(y_train, p_train, 'o', markersize=1)
    ax.plot(y_valid, p_valid, 'o', color="red", markersize=1)
    ax.plot(y_test, p_test, 'o', color="green", markersize=1)
    plt.xlim([0, 14])
    ax.set_ylim((0, 14))
    # plt.axis('equal')
    plt.title("Training(Blue) vs Validation(Red) vs Test(green) accuracy")
    plt.show()
    plt.clf()

    sb.set_style('white')
    sb.kdeplot({"labeled": y_train, "predicted": p_train}, color="blue", fill=True, bw_method=.15)
    plt.title("Training data")
    plt.show()
    plt.clf()

    sb.kdeplot({"labeled": y_valid, "predicted": p_valid}, color="red", fill=True, bw_method=.15)
    plt.title("Validation data")
    plt.show()
    plt.clf()

    sb.set_style('white')
    sb.kdeplot({"labeled": y_test, "predicted": p_test}, color="blue", fill=True, bw_method=.15)
    plt.title("Test data")
    plt.show()
