import math
import numpy as np

def read_file(file_path):
    data = []
    with open(file_path, 'r') as file:
        for line in file.readlines()[1:]:
            id, smiles, ki_nm = line.split("\t")
            if float(ki_nm) > 0:
                data.append([int(id), smiles, float(ki_nm)])
    return data[1:]


def ki_nm_to_pKi(ki_nm):
    return -math.log10(1e-9 * ki_nm)


def data_pKi_transform(data):
    return [[id, smiles, ki_nm_to_pKi(ki_nm)] for id, smiles, ki_nm in data]


def write_inputfile(data, file_path):
    with open(file_path, 'w') as file:
        for el in data:
            if isinstance(el, list):
                line = "\t".join([str(x) for x in el]) + "\n"
                file.write(line)
            else:
                file.write(str(el)+"\n")


def load_data(dir):
    x_train = np.genfromtxt(dir + '/x_train.dat', delimiter='\t')
    y_train = np.genfromtxt(dir + '/y_train.dat', delimiter='\t')
    x_valid = np.genfromtxt(dir + '/x_valid.dat', delimiter='\t')
    y_valid = np.genfromtxt(dir + '/y_valid.dat', delimiter='\t')
    return x_train, y_train, x_valid, y_valid
