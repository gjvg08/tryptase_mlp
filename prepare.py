import tensorflow as tf
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import Draw
from parser import read_file, data_pKi_transform, write_inputfile
from desc import make_descriptors_from_smiles

if __name__ == '__main__':
    train_file = read_file('TryptaseDataset/train.csv')
    train_pKi = data_pKi_transform(train_file)
    valid_file = read_file('TryptaseDataset/valid.csv')
    valid_pKi = data_pKi_transform(valid_file)

    # mol = Chem.MolFromSmiles(train_pKi[500][1])
    # print(mol)
    # Chem.Compute2DCoords(mol)
    # print(Chem.MolToMolBlock(mol))
    # img = Draw.MolToImage(mol)
    # img.save("test.png")

    # for el in (["Chi0", "Kappa1", "MolLogP"] + ["PEOE_VSA" + str(i) for i in range(1, 15)]):
    #     print(f"desc.append(Descriptors.{el}(mol)")

    # print(make_descriptors_from_smiles(train_pKi[500][1]))
    # print(len(make_descriptors_from_smiles(train_pKi[500][1])))
    #
    train_pKis = [line[-1] for line in train_pKi]
    valid_pKis = [line[-1] for line in valid_pKi]
    train_smiles = [line[1] for line in train_pKi]
    valid_smiles = [line[1] for line in valid_pKi]
    train_descs = [make_descriptors_from_smiles(smiles) for smiles in train_smiles]
    valid_descs = [make_descriptors_from_smiles(smiles) for smiles in valid_smiles]

    write_inputfile(train_descs, "data/x_train.dat")
    write_inputfile(train_pKis, "data/y_train.dat")
    write_inputfile(valid_descs, "data/x_valid.dat")
    write_inputfile(valid_pKis, "data/y_valid.dat")