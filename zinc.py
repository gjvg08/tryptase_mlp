import os
import numpy as np

def read_batch(file_path):
    batch = []
    with open(file_path, 'r') as file:
        for line in file.readlines():
            smiles = line.split()[0]
            batch.append(smiles)
    return batch


